/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-03-26 01:53:01
 * @modify date 2020-03-26 01:53:01
 * @desc [description]
 */
import { Router } from 'express';
import contract from '../controller/contract';
import account from '../controller/account';
import passport from 'passport';

import initializeDb from '../db';
import {generateAccessToken, respond, authenticate} from '../middleware/auth';

let routes = Router();

initializeDb(db => {
    routes.get('/info-contract', authenticate, contract.getData);
    routes.get('/balance-contract', authenticate, contract.getBalance);
    routes.post('/payment-contract', authenticate, contract.doPayment)

    routes.post('/register', account.register);
    routes.post('/login', passport.authenticate('local', {session: false,scope: []}), generateAccessToken, respond);
    routes.get('/logout', authenticate, account.logout);
    routes.get('/me', authenticate, account.me);

    routes.get('*', contract.notFound);  
});
    
export default routes;