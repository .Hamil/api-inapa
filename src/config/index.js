/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-03-26 01:53:01
 * @modify date 2020-03-26 01:53:01
 * @desc [description]
 */

export default {
    "port": 3000,
    "bodyLimit": "100kb",
    "inapaHost": "http://179.51.66.114",
    "version": "/desarrollo/v2/",
    "contractInfo": "?WS=datos&contrato=",
    "contractBalance": "?WS=balance&contrato=",
    "authentication": "?identificarse",
    "payContract": "?WS=pagar",
    "user": "optic_serviciosrd",
    "password": "7@9rYP8TdFP@0AKUwxlF",
    "headersBrowser": {
		'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
		'Accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
    'Content-Type': 'application/json'
	},
  "mongoUrl": "mongodb://localhost:27017/api-inapa"
}