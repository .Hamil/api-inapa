/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-03-26 01:53:01
 * @modify date 2020-03-26 01:53:01
 * @desc [description]
 */

import config from '../config';
import request from 'request-promise';

/**
 * @type Promise
 * @description returns the JWT of the INAPA
 * @param {*} req 
 * @param {*} res 
 */
const getToken = () => {
    return new Promise((resolve, reject)=>{
        let token = null;
        let options = {
            uri: config.inapaHost + config.version + config.authentication,
            method: 'POST',
            form: {
                "usuario": config.user,
                "clave": config.password
            }
        }

        request(options)
            .then(response => {
                response = JSON.parse(response);
                token = response.token;
                resolve(token);
            })
            .catch(err => {
                token = `Problems on the getToken method ---> ${err}`;
                reject(token);
            });  
    });
}

/**
 * @description returns the information of the owner of a contract
 * @param {*} req 
 * @param {*} res 
 */
const getData = (req, res) => {
    getToken()
        .then(token => {
            let options = {
                uri: config.inapaHost + config.version + config.contractInfo + req.body.contract_id,
                method: 'POST',
                form: {
                    "token": token
                }
            }
        
            request(options)
                .then(response => {
                    res.status(200).json({code: 200, message: JSON.parse(response)});
                })
                .catch(err => {
                    res.status(500).json({code: 500, message: err});
                });
        })
        .catch(err => {
            res.status(500).json({code: 500, message: err});
            console.log(err);
        });
}

/**
 * @description returns the invoices of a specific contract
 * @param {*} req 
 * @param {*} res 
 */

const getBalance = (req, res) => {
    getToken()
        .then(token => {
            let options = {
                uri: config.inapaHost + config.version + config.contractBalance + req.body.contract_id,
                method: 'POST',
                form: {
                    "token": token
                }
            }
        
            request(options)
                .then(response => {
                    res.status(200).json({code: 200, message: JSON.parse(response)});
                })
                .catch(err => {
                    res.status(500).json({code: 500, message: err});
                    console.log(`error on the request Balance, contract_id: ${req.body.contract_id}`);
                });
        })
        .catch(err => {
            res.status(500).json({code: 500, message: err});
            console.log(err);
        }); 
}
/**
 * @description this show the client that a Endpoint does not exist
 * @param {*} req 
 * @param {*} res 
 */
const notFound = (req, res) => {
    res.status(404).json({code: 404, message: "Endpoint not found"});
}

const doPayment = (req, res) => {
    
    getToken()
        .then(token => {
            let form = {
                "contrato": req.body.contract_id,
                "monto": req.body.amount,
                "token": token,
                "forma_pago": req.body.payMethod
            }

            if(req.body.payMethod === 'credito' || req.body.payMethod === 'debito'){
                form = {
                    "contrato": req.body.contract_id,
                    "monto": req.body.amount,
                    "token": token,
                    "forma_pago": req.body.payMethod,
                    "numero_tarjeta": req.body.numberCard,
                    "numero_autorizacion": req.body.numberBankAuthoritation,
                    "numero_transaccion": req.body.numberTransaccion
                }
            }
            let options = {
                uri: config.inapaHost + config.version + config.payContract,
                method: 'POST',
                form: form
            }
        
            request(options)
                .then(response => {
                    res.status(200).json({code: 200, message: JSON.parse(response)});
                })
                .catch(err => {
                    res.status(500).json({code: 500, message: err});
                });
        })
        .catch(err => {
            res.status(500).json({code: 500, message: err});
            console.log(err);
        });

}

export default({
    getData,
    getBalance,
    notFound,
    doPayment
});