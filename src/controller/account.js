/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-03-31 00:25:23
 * @modify date 2020-03-31 00:25:23
 * @desc [description]
 */


import Account from '../model/account';
import passport from 'passport';

/**
 * @description register an account for JWT Authentication
 * @param {*} req 
 * @param {*} res 
 */

const register = (req, res) => {
    Account.register(new Account({ username: req.body.email}), req.body.password, (err, account) => {
        if (err) {
            res.status(500).json({code: 500, message: err});
        }

        passport.authenticate(
            'local', {
                session: false
            })(req, res, () => {
                res.status(201).send({code: 201, message: "Succesfully created new account"});
            });
    });
}

/**
 * @description returns a JSON Web Token if the account that the client sent exist
 * @param {*} req 
 * @param {*} res 
 */

const login = (req, res) => {
    passport.authenticate(
        'local', {
            session: false,
            scope: []
        })
}

/**
 * @description destroying the token
 * @param {*} req 
 * @param {*} res 
 */

const logout = (req, res) => {
    res.logout();
    res.status(200).send({code: 200, message: 'Successfully logged out'});
}

/**
 * @description returns to the client the current user from a JWT
 * @param {*} req 
 * @param {*} res 
 */
const me = (req, res) => {
    res.status(200).json(req.user);
}

export default ({
    register,
    login,
    logout,
    me
})